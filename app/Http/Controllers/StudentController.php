<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use Redirect;

class StudentController extends Controller
{
    // view student
    public function viewStudent()
    {
        return view('form.student');
    }
    // store data information
    public function store(Request $request)
    {
        $fullName       =   $request->fullName;
        $sex            =   $request->sex;
        $email          =   $request->email;
        $phoneNumber    =   $request->phoneNumber;
        $age            =   $request->age;
        $country        =   $request->country;
        $province       =   $request->province;
        $district       =   $request->district;
        $subject        =   $request->subject;  
        $schoolYear     =   $request->schoolYear;

        $studentStore   =   [
            'full_name'      => $request->fullName,
            'sex'            => $request->sex,
            'email'          => $request->email,
            'phone_number'   => $request->phoneNumber,
            'age'            => $request->age,
            'province'       => $request->province,
            'district'       => $request->district,
            'subject'        => $request->subject,
            'school_year'    => $request->schoolYear
        ];
        DB::table('tbl_student')->insert($studentStore);
        Session::flash('message', "Data has been insert successful!.");
        return Redirect::back();
        // dd($studentStore);
    }
}
